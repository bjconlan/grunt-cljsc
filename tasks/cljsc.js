/**
 * Grunt cljsc multitask.
 *
 * @see The <a href="http://gruntjs.com">Grunt</a> build system for more details.
 *
 * @since 0.1.0
 *
 * Copyright(c) 2012 Foldr
 * EPL Licensed
 */
module.exports = function(grunt) {
	var path = require('path'),
	    jvmpin = require('jvmpin'),
	    cwd = process.cwd();

	/**
	 * This task provides the ability to communicate to a clojurescript instance hosted
	 * using the Nailgun server.
	 *
	 * The gruntjs configuration fragment is expected to be formed as follows:
	 *
	 * ...
	 * cljsc: {                        // task
	 *   dev: {                        // profile (multitask)
	 *     src: 'src/js',              // source file search path
	 *     dest: 'dest/js',            // destination root path defaults to 'out'
	 *     options: {
	 *       host: 'localhost',        // nailgun host name. defaults to 'localhost'
	 *       port: '2113'              // nailgun port. defaults to 2113
	 *       target: 'nodejs',         // defaults to standard javascript runtime
	 *       optimizations: 'simple',  // 'advanced or 'simple'. defaults to simple
	 *       prettyPrint: true,        // defaults to false
	 *       extras: ''                // you can append any extra 'edn-format' map fragment
	 *                                 // which has not been accounted for ':key :value'
	 *     },
	 *  },
	 *  release: {
	 *     src: 'src/js',              // source file search path
	 *     dest: 'dest/js',            // destination root path
	 *     options: {
	 *       target: 'nodejs',
	 *       optimizations: 'advanced'
	 *       outputTo: 'main.js'       // specify the root output file defaults to main.js
	 *     }
	 *   }
	 *   ...
	 * 
	 */
	grunt.registerMultiTask('cljsc', 'compile cljs file(s)', function() {
		var src_path = this.file.src;
		    dest_path = this.file.dest || 'out',
		    options = this.data.options || {},
		    options.host = this.data.host || 'localhost',
		    options.port = this.data.port || 2113,
		    options.extras = this.data.extras || "",
		    options.outputTo = path.join(cwd, this.data.outputTo || 'main.js'),
		    options.outputDir = path.join(cwd, dest_path),
		    ednSymbols = {
		    	'nodejs': ':nodejs',
		    	'simple': ':simple',
		    	'advanced': ':advanced',
		    	'outputTo': ':output-to',
		    	'outputDir': ':output-dir',
		    	'prettyPrint': ':pretty-print',
		    },
		    filteredOptions = {}
	//	    gruntResult = this.async();

		Object.keys(options).filter(function(key) {
			return !(key === 'host' || key === 'port' || key === 'extras');
		}).forEach(function(key) {
			filteredOptions[key] = options[key];
		});

		var argumentString = writeToEdn(filteredOptions, ednSymbols);
		argumentString = argumentString.slice(0,-1) + options.extras + '}';
		
		var client = jvmpin.createConnection(options.port, options.host);
		client.on('error', function(e) {
			grunt.fatal("Unable to connect to cljsc nailgun server", e);
		});
		
		// some huge assumptions are made here based on how the clojurescript instance
		// is initalized. For the most part we assume that it's invoked using the 
		// './bin/cljsc-ng' command meaning that the 'cljsc.clj' can be located using:
		var args = ['./bin/cljsc.clj', path.join(cwd, src_path), argumentString];
		    proc = client.spawn('clojure.main', args);

		grunt.log.debug('calling: ' + args);
		proc.stderr.on('data', function(err) { grunt.log.write(err.toString()); });
		proc.stdout.on('data', function(out) { grunt.log.write(out.toString()); });
		proc.on('exit', function(sig) { return /*gruntResult(sig == 0);*/ sig == 0; });
	});

	function writeToEdn(object, symbolMap) {
		var ednString = "{",
		    symbolMap = symbolMap || {};

		for (key in object) {
			// remap keys with '-' character as it doesn't play nice with json
			var symbol = symbolMap[key];
			if (symbol) {
				ednString += symbol + ' ';
			} else {
				ednString += ':' + key + ' ';
			}

			var value = object[key];
			switch (typeof value) {
				case 'number':
					ednString += value + ' ';
					break;
				case 'string': 
					symbol = symbolMap[value];
					if (symbol) {
						ednString += symbol + ' ';
					} else {
						ednString += '"' + value + '" ';
					}
					break;
				case 'object':
					ednString += writeToEdn(value, symbolMap);
					break;
				case 'function':
					break; // not supported (ever)
				case 'array':
					break; // not supported (lazy)
				default:
					ednString += value + ' ';
			}
		}

		return ednString += "}";
	}

}

