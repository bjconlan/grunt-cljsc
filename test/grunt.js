module.exports = function(grunt) {
	grunt.initConfig({
		cljsc: {
			simple: {
				src: 'fixtures/hello',
				dest: 'output',
				options: {
				}
			},
			advanced: {
				src: 'fixtures/hello',
				dest: 'output',
				options: {
				}
			}
		}
	});

	grunt.loadTasks('../tasks');
	grunt.registerTask('default', 'cljsc');
};
