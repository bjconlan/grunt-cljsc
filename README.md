# Grunt cljsc task (clojurescript compiler)

A grunt cljsc compiler task promoting clojurescript development under
the nodejs build tools.

If you are looking for a clojurescript development stack and are using
clojure/java I recommend that you take a good look at
(cljs-build)[https://github.com/emezeske/lein-cljsbuild] for
lein.

But if you love manually downloading and performing one off setup task
then come on down. (hopefully this will get better with time but until
then here is a quick guide.

## Usage

Currently the cljsc compiler relies significantly on manual steps in
regards to having the cljsc compiler accessible from the cljsc grunt
task.

As with any good recipe, I'll set you up with all the ingredents now.

* (Git installation)[http://git-scm.com].
* (Java +1.6 installation)[http://java.com].
* (Clojurescript)[https://github.com/clojure/clojurescript] clone (`git clone https://github.com/clojure/clojurescript.git`)
* (Nailgun +0.7.1)[http://ooo-maven.googlecode.com/hg/repository/com/martiansoftware/nailgun/0.7.1/nailgun-0.7.1.jar]

I will assume that you are versed in the installation of most of the
formentioned products for your operating system.

1. Place the nailgun-0.7.1.jar into the checkedout clojurescript/lib
   folder. This is done to add the jar onto the classpath.
2. Create a script/bat file which follows the basic layout of the cljsc
   script/bat but contains the following change:

```diff
< if test "$#" -eq 0
< then
<   echo 'Usage: cljsc <file-or-dir>'
<   echo '       cljsc <file-or-dir> "{:optimizations :advanced}"'
< else
<   java -server -cp "$CLJSC_CP" clojure.main
"$CLOJURESCRIPT_HOME/bin/cljsc.clj" "$@"
< fi
---
> java -server -cp "$CLJSC_CP" com.martiansoftware.nailgun.NGServer
```

   For those of you unfamilar with reading diff files basicaly you need
   to replace the if statement with `java -server -cp "$CLJSC_CP" 
   com.martiansoftware.nailgun.NGServer`.
   
   Then save it in the clojurescript/bin folder as something useful. I
   personally use `cljsc-ng` (and will refer to this as the file
   throughout the guide.

   Now you simply have to start the created script/bat (you might need
   to make it executable) FROM THE ROOT CLOJURESCRIPT FOLDER USING
   `./bin/cljsc-ng`. The reason why the executing folder is so important
   is due to the fact that the CLOJURESCRIPT\_HOME environment variable
   is not know by the grunt task an therefore always assumes you are
   running the server from the clojurescript root.

Now that the hard stuff is done you can use the nodejs build system to
do the rest of the hard work for you. Simply:

* Update your `package.json` dependencies:
```javascript
...
  "devDependencies": { "grunt-cljsc": "~0.1" }
...
```
* Install grunt into the system using `npm install grunt -g`.
* Create the most amazing gruntfile that does so much magic for you
  you'll forget what you ever did without it.

  This an abridged version with the important stuff for the cljsc task:
```javascript
...
  cljsc: {
    dev: {                       // dev build task 
      src: 'src/js',             // source file search path
      dest: 'dest/js',           // destination root path defaults to 'out'
      options: {
        host: 'localhost',       // nailgun host name. defaults to localhost'
        port: '2113'             // nailgun port. defaults to 2113
        target: 'nodejs',        // defaults to standard javascript
        optimizations: 'simple', // 'advanced or 'simple'. defaults to simple
        prettyPrint: true, 
        extras: ''               // any extra 'edn-format' entries that
haven been overlooked in the options.
      },
    },
    release: {
      src: 'src/js',            
      dest: 'dest/js',             
      options: {
        target: 'nodejs',
        optimizations: 'advanced',
        outputTo: 'main.js'        // specify the root output file defaults to main.js
      }
    }
  }
  ...
  grunt.loadNpmTasks('grunt-cljsc');
  grunt.registerTask('default', 'cljsc');
  ...
```

If you are having any problems it's more than likely a bug. So please
throw any issues into the bug tracker and someone will eventually deal
with them.

## License

Copyright © 2012 Foldr 

Distributed under the Eclipse Public License.
